﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public unsafe class Tile : IEquatable<Tile> {
        public const int BYTE_LENGTH = 20; // Tile data length in bytes
        public byte[] characters;
        public int x, y;

        // The "parent" tile top left, as tiles can be larger than 1x1
        public ushort topLeftTileX {
            get { return BitConverter.ToUInt16(characters, 6); }
            set {
                fixed (byte* start = &characters[6]) {
                    *(ushort*)start = value;
                }
            }
        }

        public ushort topLeftTileY {
            get { return BitConverter.ToUInt16(characters, 8); }
            set {
                fixed (byte* start = &characters[8])
                {
                    *(ushort*)start = value;
                }
            }
        }

        // This is always 0x07 for real tiles, 0x00 for dummy tiles.
        public byte unknown0 {
            get { return characters[5]; }
            set { characters[5] = value; }
        }

        public byte unknown1 {
            get { return characters[10]; }
            set { characters[10] = value; }
        }

        // unknown may be 1 if it has child tiles, 0 otherwise
        public byte unknown2 {
            get { return characters[11]; }
            set { characters[11] = value; }
        }

        public static Tile create(byte** cursor, int x, int y) {
            Tile newTile = new Tile(cursor, x, y);

            if (newTile.isMasterTile()) {
                return new MasterTile(newTile);
            } else {
                return new SlaveTile(newTile);
            };
        }

        private Tile(byte** cursor, int x, int y) {
            this.x = x;
            this.y = y;

            characters = new byte[BYTE_LENGTH];
            MemoryHelpers.memcpy(characters, *cursor, BYTE_LENGTH);
            *cursor += BYTE_LENGTH;
        }

        protected Tile() { }

        public bool isMasterTile() {
            return BitConverter.ToInt32(characters, 0) >= 0;
        }

        public byte[] serialize() {
            return characters;
        }

        public int getTileDataLength() {
            return BYTE_LENGTH;
        }

        /*public override string ToString() {
            return Encoding.ASCII.GetString(characters);
        }*/

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int memcmp(byte[] b1, byte[] b2, long count);

        public override int GetHashCode() {
            unchecked {
                if (characters == null) {
                    return 0;
                }
                int hash = 17;
                foreach (byte element in characters) {
                    hash = hash * 31 + element.GetHashCode();
                }
                return hash;
            }
        }

        public override bool Equals(object obj) { return obj is Tile && Equals((Tile)obj); }
        public bool Equals(Tile d) {
            return characters.Length == d.characters.Length && memcmp(characters, d.characters, characters.Length) == 0;
        }
    }
}
