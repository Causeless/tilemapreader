﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public unsafe class SlaveTile : Tile {
        public SlaveTile(Tile tile) {
            x = tile.x;
            y = tile.y;
            characters = tile.characters;
        }

        public int masterTileIndex {
            get {
                fixed (byte* start = &characters[0]) {
                    uint asInt = *(uint*)start;
                    return (int)(uint.MaxValue - asInt);
                }
            }
            set {
                fixed (byte* start = &characters[0]) {
                    uint* asInt = (uint*)start;
                    *asInt = (uint.MaxValue - (uint)value);
                }
            }
        }
    }
}
