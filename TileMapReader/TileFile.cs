﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public class MasterTileNotFoundException : Exception {}

    public unsafe class TileFile {
        const int MAP_WIDTH = 2032;
        const int MAP_HEIGHT = 1437;

        public List<Tile> realTiles;
        public List<Tile> dummyTiles;

        private List<string> mapIdToPaths;
        private Dictionary<string, int> mapPathsToId;

        public TileFile(byte[] data) {
            fixed (byte* start = &data[0])
            {
                byte* cursor = start;

                realTiles = new List<Tile>(tileCount);
                for (int i = 0; i < tileCount; i++) {
                    int x, y;
                    getPosFromTileIndex(i, out x, out y);
                    realTiles.Insert(i, Tile.create(&cursor, x, y));
                }

                dummyTiles = new List<Tile>(tileCount);
                for (int i = 0; i < tileCount; i++) {
                    int x, y;
                    getPosFromTileIndex(i, out x, out y);
                    dummyTiles.Insert(i, Tile.create(&cursor, x, y));
                }

                int mapCount = *(int*)cursor;
                cursor += sizeof(int);

                mapIdToPaths = new List<string>(mapCount);
                mapPathsToId = new Dictionary<string, int>(mapCount);
                for (int i = 0; i < mapCount; i++) {
                    // Map paths are 1-indexed, not 0-indexed
                    string mapPath = new LengthPrependedASCII(&cursor).ToString();
                    addMapPath(mapPath);
                }
            }
        }

        public byte[] serialize() {
            byte[] realTilesData = new byte[tileCount*Tile.BYTE_LENGTH];
            for (int i = 0; i < tileCount; i++) {
                byte[] currentTile = realTiles[i].serialize();
                Array.Copy(currentTile, 0, realTilesData, i*Tile.BYTE_LENGTH, Tile.BYTE_LENGTH);
            }

            byte[] dummyTilesData = new byte[tileCount*Tile.BYTE_LENGTH];
            for (int i = 0; i < tileCount; i++) {
                byte[] currentTile = dummyTiles[i].serialize();
                Array.Copy(currentTile, 0, dummyTilesData, i*Tile.BYTE_LENGTH, Tile.BYTE_LENGTH);
            }

            int mapCount = mapIdToPaths.Count();
            List<byte> mapPathsData = new List<byte>();
            for (int i = 0; i < mapCount; i++) {
                byte[] currentMapPath = new LengthPrependedASCII(mapIdToPaths[i]).serialize();
                mapPathsData.AddRange(currentMapPath);
            }

            // Now add it all together
            int fileLength = realTilesData.Count()+dummyTilesData.Count()+sizeof(int)+mapPathsData.Count();
            byte[] fileData = new byte[fileLength];

            fixed (byte* start = &fileData[0])
            {
                byte* cursor = start;

                Array.Copy(realTilesData, 0, fileData, 0, realTilesData.Length);
                cursor += realTilesData.Length;

                Array.Copy(dummyTilesData, 0, fileData, (int)(cursor-start), dummyTilesData.Length);
                cursor += dummyTilesData.Length;

                *(int*)cursor = mapCount;
                cursor += sizeof(int);

                mapPathsData.CopyTo(fileData, (int)(cursor-start));
            }

            return fileData;
        }

        // Returns true if new map path was added, false if already exists
        public bool addMapPath(string mapPath) {
            // Don't add if the map path already exists
            if (mapPathsToId.ContainsKey(mapPath)) { return false; }
            
            // We want id at Count as default, (1 more than max element), as map paths are 1-indexed
            mapIdToPaths.Add(mapPath);
            int id = mapIdToPaths.Count;

            mapPathsToId[mapPath] = id;
            return true;
        }

        public void removeMapPath(string mapPath) {
            // SHOULD NOT CALL! THIS WILL PROBABLY BREAK THINGS
            // Only should be used once better support for assigning tiles is done to ensure map path isn't being used
            throw new NotSupportedException();

            mapIdToPaths.Remove(mapPath);
            mapPathsToId.Clear();

            for (int i = 0; i < mapIdToPaths.Count; i++) {
                string currentPath = mapIdToPaths[i];
                mapPathsToId[mapPath] = i + 1; // i + 1 as map paths are 1-indexed
            }
        }

        public string getMapPath(MasterTile tile) {
            return getMapPath(getMapId(tile));
        }

        public string getMapPath(int id) {
            return mapIdToPaths[id - 1];
        }

        public void setMap(MasterTile tile, string mapPath) {
            tile.mapId = getMapId(mapPath);
        }

        public int getMapId(MasterTile tile) {
            return tile.mapId;
        }

        public int getMapId(string mapPath) {
            return mapPathsToId[mapPath];
        }

        public Tile getTile(int x, int y) {
            return realTiles[getTileIndexFromPos(x, y)];
        }

        private class TileByPos : Tile {
            public TileByPos(Tile tile) {
                x = tile.x;
                y = tile.y;
                characters = tile.characters;
            }

            public override int GetHashCode() {
                unchecked {
                    int hash = 17;
                    hash = hash * 31 + x;
                    hash = hash * 31 + y;
                    return hash;
                }
            }

            public override bool Equals(object obj) { return obj is TileByPos && Equals((TileByPos)obj); }
            public bool Equals(TileByPos d) { return x == d.x && y == d.y; }
        }

        public MasterTile getMasterTileFromTopLeft(int x, int y) {
            // The most that'll ever be in the queue at once is the cross-section of the map tiles
            // If we assume a max of 8x8, then the most that'll ever be in the queue is 9
            // As the search will go down diagonally until it hits the bottom right corner
            Queue<TileByPos> tilesToCheck = new Queue<TileByPos>(8);
            HashSet<TileByPos> checkedTiles = new HashSet<TileByPos>();

            // Create action to add tiles
            Action<TileByPos> queueTile = delegate (TileByPos t) {
                bool t1 = t.topLeftTileX == x;
                bool t2 = t.topLeftTileY == y;
                bool t3 = !checkedTiles.Contains(t);
                if (t.topLeftTileX == x && t.topLeftTileY == y && !checkedTiles.Contains(t)) {
                    tilesToCheck.Enqueue(t);
                    checkedTiles.Add(t);
                }
            };

            queueTile(new TileByPos(getTile(x, y)));
            while (true) {
                if (tilesToCheck.Count == 0) {
                    throw new MasterTileNotFoundException();
                }

                TileByPos t = tilesToCheck.Dequeue();
                if (t.isMasterTile()) {
                    return new MasterTile(t);
                }

                // Current tile isn't the master, so add the tile below and to the right of it
                queueTile(new TileByPos(getTile(t.x+1, t.y)));
                queueTile(new TileByPos(getTile(t.x, t.y+1)));
            }
        }

        // This almost certainly breaks with overlapping tiles
        public void rotateTilesFromTopLeft(int x, int y, int degrees) {
            MasterTile masterTile = getMasterTileFromTopLeft(x, y);
            SlaveTile anySlaveTile = (SlaveTile)realTiles.Where(t => !(t is MasterTile)).FirstOrDefault();

            byte[] masterCopy = new byte[masterTile.characters.Length];
            Array.Copy(masterTile.characters, masterCopy, masterTile.characters.Length);

            // Replace master tile as slave
            Array.Copy(anySlaveTile.characters ?? masterTile.characters, masterTile.characters, masterTile.characters.Length);

            // Now find the tiles that are part of this tileset
            var tileset = realTiles.Where(t => t.topLeftTileX == x && t.topLeftTileY == y);

            // Now find and set the top left / top right / bottom right / bottom left depending on rotation
            Tile newMasterTile;
            switch (degrees) {
                case 0:
                    newMasterTile = getTile(tileset.Min(t2 => t2.x), tileset.Min(t => t.y));
                    break;
                case 90:
                    newMasterTile = getTile(tileset.Max(t2 => t2.x), tileset.Min(t => t.y));
                    break;
                case 180:
                    newMasterTile = getTile(tileset.Max(t2 => t2.x), tileset.Max(t => t.y));
                    break;
                case 270:
                    newMasterTile = getTile(tileset.Min(t2 => t2.x), tileset.Max(t => t.y));
                    break;
                default:
                    throw new InvalidTileRotationException(degrees);
            }
            Array.Copy(masterCopy, newMasterTile.characters, masterCopy.Length);

            MasterTile asMasterTile = new MasterTile(newMasterTile);
            asMasterTile.setRotationInDegrees(degrees);

            // Now update master tile index
            // For some reason breaks things, so is disabled
            /*var slaveTiles = tileset.Where(t => !(t is MasterTile));
            foreach (SlaveTile t in slaveTiles) {
                t.masterTileIndex = getTileIndexFromPos(asMasterTile.x, asMasterTile.y);
            }*/
        }

        public Tuple<int, int> getImagePosFromCoord(float x, float y) {
            int tileX = (int)(mapWidth * x);
            int tileY = (int)(mapHeight * y);

            return Tuple.Create(tileX, tileY);
        }

        public Tuple<float, float> getCoordFromImagePos(int x, int y) {
            float coordX = (float)x / mapWidth;
            float coordY = (float)y / mapHeight;

            return Tuple.Create(coordX, coordY);
        }

        public int mapWidth { get { return MAP_WIDTH; } private set { } }
        public int mapHeight { get { return MAP_HEIGHT; } private set { } }
        public int tileCount { get { return MAP_WIDTH * MAP_HEIGHT; } private set { } }

        private int getTileIndexFromPos(int x, int y) {
            return y*mapWidth + x;
        }

        private void getPosFromTileIndex(int i, out int x, out int y) {
            y = i / mapWidth;
            x = i % mapWidth;
        }
    }
}
