﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public static unsafe class MemoryHelpers {
        public static void memcpy(byte* dest, byte* src, int length) {
            for (uint i = 0; i < length; i++) {
                dest[i] = src[i];
            }
        }

        public static void memcpy(byte* dest, byte[] src, int length) {
            for (uint i = 0; i < length; i++) {
                dest[i] = src[i];
            }
        }

        public static void memcpy(byte[] dest, byte* src, int length) {
            for (uint i = 0; i < length; i++) {
                dest[i] = src[i];
            }
        }
    }
}
