﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public class InvalidTileRotationException : Exception {
        public int rotation;

        public InvalidTileRotationException(int rotation) {
            this.rotation = rotation;
        }

        private InvalidTileRotationException() { }
    }

    public unsafe class MasterTile : Tile {
        public MasterTile(Tile tile) {
            x = tile.x;
            y = tile.y;
            characters = tile.characters;
        }

        public int mapId {
            get { return BitConverter.ToInt32(characters, 0); }
            set {
                fixed (byte* start = &characters[0]) {
                    *(int*)start = value;
                }
            }
        }

        public byte direction {
            get { return characters[4]; }
            set { characters[4] = value; }
        }

        public int getRotationInDegrees() {
            switch (direction) {
                case 0x10:
                    return 0;
                case 0x20:
                    return 90;
                case 0x40:
                    return 180;
                case 0x80:
                    return 270;
                default:
                    throw new InvalidTileRotationException(direction);
            }
        }

        public void setRotationInDegrees(int degrees) {
            switch (degrees) {
                case 0:
                    direction = 0x10;
                    break;
                case 90:
                    direction = 0x20;
                    break;
                case 180:
                    direction = 0x40;
                    break;
                case 270:
                    direction = 0x80;
                    break;
                default:
                    throw new InvalidTileRotationException(degrees);
            }
        }
    }
}
