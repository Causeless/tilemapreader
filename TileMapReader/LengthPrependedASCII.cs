﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMapReader {
    public unsafe class LengthPrependedASCII : IEquatable<LengthPrependedASCII> {
        public byte[] characters;

        public LengthPrependedASCII(byte** cursor) {
            // Parse the length
            ushort length = *(ushort*)*cursor;
            *cursor += sizeof(ushort);

            characters = new byte[length];
            MemoryHelpers.memcpy(characters, *cursor, length);
            *cursor += length;
        }

        public LengthPrependedASCII(string str) {
            characters = Encoding.ASCII.GetBytes(str);
        }

        public byte[] serialize() {
            ushort length = (ushort)characters.Length;

            byte[] data = new byte[length+sizeof(ushort)];

            fixed (byte* start = &data[0])
            {
                byte* cursor = start;

                *(ushort*)cursor = length;
                cursor += sizeof(ushort);

                MemoryHelpers.memcpy(cursor, characters, length);
            }

            return data;
        }

        public override string ToString() {
            return Encoding.ASCII.GetString(characters);
        }

        public override int GetHashCode() { return this.ToString().GetHashCode(); }
        public override bool Equals(object obj) { return obj is LengthPrependedASCII && Equals((LengthPrependedASCII)obj); }
        public bool Equals(LengthPrependedASCII p) { return this.ToString() == p.ToString(); }
    }
}
